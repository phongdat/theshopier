<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();

//Bootstrap classes

$__columns = absint($woocommerce_loop['columns']);

$resp = array();
if( absint($__columns) > 1 ) {
	$resp[] = 'col-lg-' . round( 24 /$__columns );
	$resp[] = 'col-md-' . round( 24 / round( $__columns * 992 / 1200) );
	$resp[] = 'col-sm-' . round( 24 / round( $__columns * 768 / 1200) );
	$resp[] = 'col-xs-' . round( 24 / round( $__columns * 480 / 1200) );
	$resp[] = 'col-mb-12';
} else {
	$resp[] = 'col-lg-24';
	$resp[] = 'col-md-24';
	$resp[] = 'col-sm-24';
	$resp[] = 'col-xs-24';
	$resp[] = 'col-mb-24';
}

$resp = apply_filters('theshopier_shop_loop_res_classes', $resp, $__columns);

$classes = array_merge($classes, $resp);

if( is_ajax() ) $classes[] = 'product';

if ( 0 === ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 === $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 === $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}
?>
<section <?php post_class( $classes ); ?>>
	<div class="product-inner">
		<div class="product-thumbnail-wrapper">
			<?php
			/**
			 * woocommerce_before_shop_loop_item hook.
			 *
			 * @hooked woocommerce_template_loop_product_link_open - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item' );?>
			<a href="<?php the_permalink(); ?>">
				<?php
				/**
				 * woocommerce_before_shop_loop_item_title hook.
				 *
				 * @hooked woocommerce_show_product_loop_sale_flash - 10
				 * @hooked woocommerce_template_loop_product_thumbnail - 10
				 */
				do_action( 'woocommerce_before_shop_loop_item_title' );
				?>
			</a>
		</div>

		<div class="product-meta-wrapper">

			<?php
			/**
			 * woocommerce_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_template_loop_product_title - 10
			 */
			do_action( 'woocommerce_shop_loop_item_title' );

			/**
			 * woocommerce_after_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );

			/**
			 * woocommerce_after_shop_loop_item hook.
			 *
			 * @hooked woocommerce_template_loop_product_link_close - 5
			 * @hooked woocommerce_template_loop_add_to_cart - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item' );

			?>

		</div>
	</div>
</section>
