<?php
/**
 * My Account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices(); ?>

<?php do_action( 'theshopier_woocommerce_before_grid_box' ); ?>

<p class="myaccount_user">
	<?php
	printf(
		wp_kses(__( 'Hello <strong>%1$s</strong> (not %1$s? <a href="%2$s" class="signout">Sign out</a>).', 'theshopier' ), array('strong'=>array(), 'a'=>array('href'=>array(), 'title'=>array(), 'class'=>array()))) . ' ',
		$current_user->display_name,
		wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
	);

	printf( wp_kses(__( 'From your account dashboard you can view your recent orders, manage your shipping and billing addresses and <a href="%s">edit your password and account details</a>.', 'theshopier' ), array('a'=>array('href'=>array(), 'title'=>array()))),
		wc_customer_edit_account_url()
	);
	?>
</p>

<?php do_action( 'woocommerce_before_my_account' ); ?>

<?php wc_get_template( 'myaccount/my-downloads.php' ); ?>

<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>

<?php wc_get_template( 'myaccount/my-address.php' ); ?>

<?php do_action( 'woocommerce_after_my_account' ); ?>

<?php do_action( 'theshopier_woocommerce_after_grid_box' ); ?>
