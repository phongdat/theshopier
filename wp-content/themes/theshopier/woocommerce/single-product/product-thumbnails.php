<?php
/**
 * Single Product Thumbnails
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_attachment_ids();
array_unshift( $attachment_ids, get_post_thumbnail_id() );

$attachment_ids = apply_filters('theshopier_woocommerce_product_gallery', $attachment_ids, $post->ID);

if ( $attachment_ids ) {
	$loop 		= 0;
	$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 4);
	//$columns = round( $columns * (6/2.5) );
	
	$options = array(
		"items"			=> $columns,
		"loop"			=> false,
		'responsive'	=> array(
			0	=> array(
				'items'	=> round( $columns * (768 / 1200) ),
			),
			480	=> array(
				'items'	=> round( $columns * (980 / 1200) ),
			),
			769	=> array(
				'items'	=> $columns
			),
			981	=> array(
				'items'	=> $columns
			),
		)
	);

	?>
	<div id="nth_prod_thumbnail" class="thumbnails <?php echo 'columns-' . $columns; ?> nth-owlCarousel loading" data-options="<?php echo esc_attr(json_encode($options));?>"><?php

		foreach ( $attachment_ids as $attachment_id ) {

			$classes = array( 'zoom', 'img_thumb' );

			if( $loop == 0 ) $classes[] = "first active";
			
			if ( $loop % $columns == 0 ) $classes[] = 'first';
			
			if ( ( $loop + 1 ) % $columns == 0 )
				$classes[] = 'last';
			
			$classes = apply_filters('theshopier_woocommerce_product_gallery_class', $classes, $attachment_id, $post->ID);
			
			$image_link = wp_get_attachment_url( $attachment_id );
			$image_small_link = wp_get_attachment_image_src( $attachment_id, 'shop_single' );

			if ( ! $image_link )
				continue;

			$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );
			$image_title = esc_attr( get_the_title( $attachment_id ) );
			/////
			
			if( !is_singular('product') && false ) {
				$image_class = esc_attr( implode( ' ', $classes ) );
				echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s" title="%s" data-rel="prettyPhoto[product-gallery]">%s</a>', $image_link, $image_class, $image_title, $image ), $attachment_id, $post->ID, $image_class );
			} else {
				$k = array_search( 'zoom', $classes );
				if( isset($k) ) unset( $classes[$k] );
				$image_class = esc_attr( implode( ' ', $classes ) );

				echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="#product-item-%s" data-pos="%s" class="%s" title="%s">%s</a>', $loop, $loop, $image_class, $image_title, $image ), $attachment_id, $post->ID, $image_class );
			}
			

			$loop++;
		}
		
		do_action('theshopier_after_woocommerce_product_thumnail', $loop);
		

	?></div><?php
}
