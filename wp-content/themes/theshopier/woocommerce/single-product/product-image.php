<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;

?>
<div class="images">

	<?php
	if ( has_post_thumbnail() ) {

		$attachment_ids = $product->get_gallery_attachment_ids();
		array_unshift($attachment_ids, get_post_thumbnail_id());

		$options = array(
			"items"			=> 1,
			"loop"			=> false,
			"autoHeight"		=> true,
			"video"				=> true,
			"videoHeight"		=> 300
		);

		printf('<div class="nth-owlCarousel p_image loading" data-options="%1$s">', esc_attr(json_encode($options)));
		$i = 1;
		foreach( $attachment_ids as $attachment_id ){
			$image_link  	= wp_get_attachment_url( $attachment_id );
			$image_title 	= esc_attr( get_the_title( $attachment_id ) );
			$image       	= wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), 0, array(
				'title'	=> $image_title,
				'alt'	=> $image_title,
				'data-zoom-image'	=> $image_link
			) );

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div data-hash="%s" class="item" style="overflow: hidden;"><a href="%s" itemprop="image" class="woocommerce-main-image  zoom1" title="%s" >%s</a><a href="%s" class="zoom icon-nth-search btn_zoom" data-rel="prettyPhoto[product-gallery]"></a></div>', $i++, $image_link, $image_title, $image, $image_link ), $attachment_id );
		}

		do_action('theshopier_after_woocommerce_product_image');

		echo "</div><!--close image .nth-owlCarousel-->";

	} else {
		sprintf ('<a href="%s" class="zoom icon-nth-search  btn_zoom"> </a>', wc_placeholder_img_src() );
		echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), esc_attr__( 'Placeholder', 'theshopier' ) ), $post->ID );

	}
	?>

	<?php do_action( 'woocommerce_product_thumbnails' ); ?>

</div>
