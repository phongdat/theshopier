<?php 
// **********************************************************************// 
// ! Register Nexthemes Widgets
// **********************************************************************// 

class Theshopier_ProductFilter_Widget extends Theshopier_Widget {

    public function __construct() {
        $this->widget_cssclass    = 'nth-widgets nth-product-filters-widget';
		$this->widget_description = esc_html__( "Shows a custom attribute in a widget which lets you narrow down the list of products when viewing product categories.", 'theshopier' );
		$this->widget_id          = 'theshopier_product_filters';
		$this->widget_name        = esc_html__('WooCommerce Layered Nav', 'theshopier' );
		
		parent::__construct();
    }
	
	public function form( $instance ) {
		$this->init_settings();
		parent::form( $instance );
	}
	
	public function update( $new_instance, $old_instance ) {
		$this->init_settings();

		return parent::update( $new_instance, $old_instance );
	}
	
	
	public function init_settings(){
		$attribute_array      = array();
		$attribute_taxonomies = wc_get_attribute_taxonomies();
		if ( $attribute_taxonomies ) {
			foreach ( $attribute_taxonomies as $tax ) {
				if ( taxonomy_exists( wc_attribute_taxonomy_name( $tax->attribute_name ) ) ) {
					$attribute_array[ $tax->attribute_name ] = isset( $tax->attribute_label ) && strlen( $tax->attribute_label ) > 0 ? $tax->attribute_label: $tax->attribute_name;
				}
			}
		}
		$this->settings = array(
			'title' => array(
				'type'  => 'text',
				'std'   => esc_html__( 'Filter by', 'theshopier' ),
				'label' => esc_html__( 'Title', 'theshopier' )
			),
			'attribute' => array(
				'type'    => 'select',
				'std'     => '',
				'label'   => esc_html__( 'Attribute', 'theshopier' ),
				'options' => $attribute_array
			),
			'display_type' => array(
				'type'    => 'select',
				'std'     => 'list-vertical',
				'label'   => esc_html__( 'Display type', 'theshopier' ),
				'options' => array(
					'list-vertical' 	=> esc_html__( 'List Vertical', 'theshopier' ),
					'list-horizontal'	=> esc_html__( 'List Horizontal', 'theshopier' )
				)
			),
			'query_type' => array(
				'type'    => 'select',
				'std'     => 'and',
				'label'   => esc_html__( 'Query type', 'theshopier' ),
				'options' => array(
					'and' => esc_html__( 'AND', 'theshopier' ),
					'or'  => esc_html__( 'OR', 'theshopier' )
				)
			),
		);
	}

    public function widget( $args, $instance ) {
		global $_chosen_attributes;

		if ( ! is_post_type_archive( 'product' ) && ! is_tax( get_object_taxonomies( 'product' ) ) ) {
			return;
		}

		$current_term = is_tax() ? get_queried_object()->term_id : '';
		$current_tax  = is_tax() ? get_queried_object()->taxonomy : '';
		$taxonomy     = isset( $instance['attribute'] ) ? wc_attribute_taxonomy_name( $instance['attribute'] ) : $this->settings['attribute']['std'];
		$query_type   = isset( $instance['query_type'] ) ? $instance['query_type'] : $this->settings['query_type']['std'];
		$display_type = isset( $instance['display_type'] ) ? $instance['display_type'] : $this->settings['display_type']['std'];
		
		if ( ! taxonomy_exists( $taxonomy ) ) {
			return;
		}
		
		$get_terms_args = array( 'hide_empty' => '1' );

		$orderby = wc_attribute_orderby( $taxonomy );

		switch ( $orderby ) {
			case 'name' :
				$get_terms_args['orderby']    = 'name';
				$get_terms_args['menu_order'] = false;
			break;
			case 'id' :
				$get_terms_args['orderby']    = 'id';
				$get_terms_args['order']      = 'ASC';
				$get_terms_args['menu_order'] = false;
			break;
			case 'menu_order' :
				$get_terms_args['menu_order'] = 'ASC';
			break;
		}

		$terms = get_terms( $taxonomy, $get_terms_args );

		if ( 0 < count( $terms ) ) {

			ob_start();

			$found = false;
			$is_color = false;

			$this->widget_start( $args, $instance );

			// Force found when option is selected - do not force found on taxonomy attributes
			if ( ! is_tax() && is_array( $_chosen_attributes ) && array_key_exists( $taxonomy, $_chosen_attributes ) ) {
				$found = true;
			}


				// List display
			$ul_class = array();
			if( strcmp( trim($taxonomy), wc_attribute_taxonomy_name( 'color' ) ) == 0 ) {
				$is_color = true;
				$ul_class[] = 'filter-by-color';
			}
			
			$ul_class[] = isset( $display_type )? $display_type: '';
			
			echo '<ul class="'. esc_attr( implode( ' ', $ul_class ) ) .'">';

			foreach ( $terms as $term ) {

				// Get count based on current view - uses transients
				$transient_name = 'wc_ln_count_' . md5( sanitize_key( $taxonomy ) . sanitize_key( $term->term_taxonomy_id ) );

				if ( false === ( $_products_in_term = get_transient( $transient_name ) ) ) {

					$_products_in_term = get_objects_in_term( $term->term_id, $taxonomy );

					set_transient( $transient_name, $_products_in_term );
				}

				$option_is_set = ( isset( $_chosen_attributes[ $taxonomy ] ) && in_array( $term->term_id, $_chosen_attributes[ $taxonomy ]['terms'] ) );
					// skip the term for the current archive
				if ( $current_term == $term->term_id ) {
					continue;
				}

				// If this is an AND query, only show options with count > 0
				if ( 'and' == $query_type ) {

					$count = sizeof( array_intersect( $_products_in_term, WC()->query->filtered_product_ids ) );

					if ( 0 < $count && $current_term !== $term->term_id ) {
						$found = true;
					}

					if ( 0 == $count && ! $option_is_set ) {
						continue;
					}

				// If this is an OR query, show all options so search can be expanded
				} else {
					$count = sizeof( array_intersect( $_products_in_term, WC()->query->unfiltered_product_ids ) );

					if ( 0 < $count ) {
						$found = true;
					}
				}

				$arg = 'filter_' . sanitize_title( $instance['attribute'] );

				$current_filter = ( isset( $_GET[ $arg ] ) ) ? explode( ',', $_GET[ $arg ] ) : array();

				if ( ! is_array( $current_filter ) ) {
					$current_filter = array();
				}

				$current_filter = array_map( 'esc_attr', $current_filter );

				if ( ! in_array( $term->term_id, $current_filter ) ) {
					$current_filter[] = $term->term_id;
				}

				// Base Link decided by current page
				if ( defined( 'SHOP_IS_ON_FRONT' ) ) {
					$link = home_url();
				} elseif ( is_post_type_archive( 'product' ) || is_page( wc_get_page_id('shop') ) ) {
					$link = get_post_type_archive_link( 'product' );
				} else {
					$link = get_term_link( get_query_var('term'), get_query_var('taxonomy') );
				}

				// All current filters
				if ( $_chosen_attributes ) {
					foreach ( $_chosen_attributes as $name => $data ) {
						if ( $name !== $taxonomy ) {

							// Exclude query arg for current term archive term
							while ( in_array( $current_term, $data['terms'] ) ) {
								$key = array_search( $current_term, $data );
								unset( $data['terms'][$key] );
							}

							// Remove pa_ and sanitize
							$filter_name = sanitize_title( str_replace( 'pa_', '', $name ) );

							if ( ! empty( $data['terms'] ) ) {
								$link = add_query_arg( 'filter_' . $filter_name, implode( ',', $data['terms'] ), $link );
							}

							if ( 'or' == $data['query_type'] ) {
								$link = add_query_arg( 'query_type_' . $filter_name, 'or', $link );
							}
						}
					}
				}

				// Min/Max
				if ( isset( $_GET['min_price'] ) ) {
					$link = add_query_arg( 'min_price', $_GET['min_price'], $link );
				}

				if ( isset( $_GET['max_price'] ) ) {
					$link = add_query_arg( 'max_price', $_GET['max_price'], $link );
				}

				// Orderby
				if ( isset( $_GET['orderby'] ) ) {
					$link = add_query_arg( 'orderby', $_GET['orderby'], $link );
				}

				// Current Filter = this widget
				if ( isset( $_chosen_attributes[ $taxonomy ] ) && is_array( $_chosen_attributes[ $taxonomy ]['terms'] ) && in_array( $term->term_id, $_chosen_attributes[ $taxonomy ]['terms'] ) ) {

					$class = 'class="chosen"';
                    $_attr_none_color_list = " fa-check-square-o";

					// Remove this term is $current_filter has more than 1 term filtered
					if ( sizeof( $current_filter ) > 1 ) {
						$current_filter_without_this = array_diff( $current_filter, array( $term->term_id ) );
						$link = add_query_arg( $arg, implode( ',', $current_filter_without_this ), $link );
					}

				} else {
                    $_attr_none_color_list = " fa-square-o";
					$class = '';
					$link = add_query_arg( $arg, implode( ',', $current_filter ), $link );

				}

				// Search Arg
				if ( get_search_query() ) {
					$link = add_query_arg( 's', get_search_query(), $link );
				}

				// Post Type Arg
				if ( isset( $_GET['post_type'] ) ) {
					$link = add_query_arg( 'post_type', $_GET['post_type'], $link );
				}

				// Query type Arg
				if ( $query_type == 'or' && ! ( sizeof( $current_filter ) == 1 && isset( $_chosen_attributes[ $taxonomy ]['terms'] ) && is_array( $_chosen_attributes[ $taxonomy ]['terms'] ) && in_array( $term->term_id, $_chosen_attributes[ $taxonomy ]['terms'] ) ) ) {
					$link = add_query_arg( 'query_type_' . sanitize_title( $instance['attribute'] ), 'or', $link );
				}
				
				if( $is_color ) {
					$datas = get_metadata( 'woocommerce_term', $term->term_id, "nth_pa_color", true );
				} else {
					$datas = '';
				}
				
				if( strcmp( trim($display_type), 'list-horizontal' ) == 0 && strlen( $datas ) > 0 ) {
					$li_style = ' style="background-color: '. $datas .';"';
				} else {
					$li_style = '';
				}

				echo '<li ' . $class . $li_style . '>';
				
				echo ( $count > 0 || $option_is_set ) ? '<a href="' . esc_url( apply_filters( 'woocommerce_layered_nav_link', $link ) ) . '">' : '<span>';
				
				if( strlen( $datas ) > 0 && strcmp( trim($display_type), 'list-vertical' ) == 0 ) {
					echo '<span style="background-color: '. esc_attr( $datas ) .'">' . esc_attr( $term->name ) . '</span>';
				} elseif( strcmp( trim($display_type), 'list-vertical' ) == 0 ) {
					//echo '<input type="checkbox" name="taxonomy-'.$taxonomy.'['.$term->term_id.'][]" />';
                    echo '<i class="fa'. esc_attr( $_attr_none_color_list ) .'"></i>';
				}
				
				echo $term->name;

				echo ( $count > 0 || $option_is_set ) ? '</a>' : '</span>';
				
				if( strcmp( trim($display_type), 'list-vertical' ) == 0 )
					echo ' <span class="count">(' . $count . ')</span></li>';

			}

			echo '</ul>';

			

			$this->widget_end( $args );

			if ( ! $found ) {
				ob_end_clean();
			} else {
				echo ob_get_clean();
			}
		}
	}
}
