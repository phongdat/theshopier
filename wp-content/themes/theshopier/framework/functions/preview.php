<?php
/**
 * Package: TheShopier.
 * User: kinhdon
 * Date: 1/29/2016
 * Vertion: 1.0
 */

add_action('theshopier_footer_init', 'theshopier_preview_block', 99);

if( !function_exists('theshopier_preview_block') ) {
    function theshopier_preview_block(){
        $server_args = array('192.168.1.110', 'demo.nexthemes.com');
        if(!in_array( $_SERVER['SERVER_NAME'], $server_args )) return null;

        /*$_preview_link = "http://demo.nexthemes.com/wordpress/images/theshopier/previews/";*/
        $_preview_link = get_template_directory_uri() . '/images/preview/';
        $gg_link = add_query_arg('key', 'AIzaSyASE5lr3VjImcJu6zjCMS91QK63zVSlcrk', 'https://www.googleapis.com/webfonts/v1/webfonts');
        $gg_font = @wp_remote_get($gg_link, array('sslverify' => false));
        $gg_fonts_data = array( '' => __('--Select a font--', 'theshopier'));
        if ( ! is_wp_error( $gg_font ) && $gg_font['response']['code'] == 200 ) {
            $gg_font = json_decode( $gg_font['body'] );
            foreach ( $gg_font->items as $font ) {
                $gg_fonts_data[ esc_attr($font->family) ] = esc_attr($font->family);
            }
        }

        ?>
        <div class="nth-preview-wrapper hidden-sm hidden-xs nth-close">
            <div class="button-wrapper">
                <div class="button-item themeforest-link"><a href="http://themeforest.net/item/shopier-responsive-multipurpose-wordpress-woocommerce-theme/15041162" target="_blank" title="Buy theme now">Purchase Now</a></div>
                <div class="button-item preview-toggle"><i class="fa fa-sliders"></i></div>
            </div>
            <div class="nth-preview-inner">
                <div class="nth-preview-container">
                    <div class="nth-preview-section layout-section">
                        <h3 class="text-center">Layout</h3>
                        <div class="layout-button text-center">
                            <?php
                            $bg_color = get_theme_mod('background_color', 'ffffff');
                            ?>
                            <button class="button wide active" data-color="<?php echo esc_attr($bg_color)?>" data-action="wide">Wide</button>
                            <button class="button boxed" data-color="<?php echo esc_attr($bg_color)?>" data-action="boxed">Boxed</button>
                        </div>
                        <div class="layout-background">
                            <h3 class="text-center">Boxed backgrounds</h3>
                            <?php
                            $bg_args = array(
                                'bg_repeat1' => array(
                                    'background-repeat'  => 'repeat',
                                    'background-position'   => 'left top',
                                    'background-size'       => 'inherit',
                                    'background-attachment' => 'fixed',
                                ),
                                'bg_repeat2' => array(
                                    'background-repeat'  => 'repeat',
                                    'background-position'   => 'left top',
                                    'background-size'       => 'inherit',
                                    'background-attachment' => 'fixed',
                                ),
                                'bg_repeat3' => array(
                                    'background-repeat'  => 'repeat',
                                    'background-position'   => 'left top',
                                    'background-size'       => 'inherit',
                                    'background-attachment' => 'fixed',
                                ),
                                'bg_full1' => array(
                                    'background-repeat'  => 'no-repeat',
                                    'background-position'   => 'center top',
                                    'background-size'       => 'cover',
                                    'background-attachment' => 'fixed',
                                ),
                                'bg_full2' => array(
                                    'background-repeat'  => 'no-repeat',
                                    'background-position'   => 'center bottom',
                                    'background-size'       => 'cover',
                                    'background-attachment' => 'fixed',
                                ),
                                'bg_full3' => array(
                                    'background-repeat'  => 'no-repeat',
                                    'background-position'   => 'center bottom',
                                    'background-size'       => 'cover',
                                    'background-attachment' => 'fixed',
                                ),
                                'bg_full4' => array(
                                    'background-repeat'  => 'no-repeat',
                                    'background-position'   => 'center top',
                                    'background-size'       => 'cover',
                                    'background-attachment' => 'fixed',
                                )
                            );
                            foreach($bg_args as $k => $v) :
                                $bg_url = $_preview_link . "{$k}.jpg";
                                $bg_json = $v;
                                $bg_json['background-image'] = 'url('.esc_url($bg_url).')';
                                ?>
                                <a href="#" class="bg-item <?php echo esc_attr($k);?>" data-bg="<?php echo esc_attr(wp_json_encode($bg_json));?>"></a>
                            <?php endforeach;?>
                        </div>
                        <div class="layout-font">
                            <h3 class="text-center">Heading Font</h3>
                            <?php
                            if(!empty($gg_fonts_data)) {
                                echo '<select name="theshopier_preview_font" id="theshopier_preview_font">';
                                foreach($gg_fonts_data as $k => $v){
                                    printf('<option value="%s">%s</option>', esc_attr($k), esc_html($v));
                                }
                                echo '</select>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="nth-preview-section homes-preview-wrapper">

                        <div class="homepage-title">
                            <h3 class="text-center">TheShopier Homepages</h3>
                        </div>

                        <?php
                        $home_k = array(
                            11 => 'Fashion Shop',
                            10 => 'Furniture Shop',
                            9 => 'Lingerie Shop',
                            8 => 'Jewelry Shop',
                            7 => 'Bike Shop',
                            6 => 'Computer Peripherals Shop',
                            5 => 'Deals Market',
                            4 => 'Overflowed Market',
                            3 => 'Sidebared Market',
                            2 => 'Parallax Market',
                            1 => 'Classic Market'
                        );
                        foreach($home_k as $k => $title) :
                            $href = "http://demo.nexthemes.com/wordpress/theshopier/home{$k}/";
                            $src = THEME_IMG_URI . "preview/preview-{$k}.jpg";
                            $class = array('home-item');
                            if(absint($k) > 10) $class[] = 'home-new';
                            ?>
                            <div class="<?php echo esc_attr(implode(' ', $class))?>">
                                <a target="_blank" title="<?php echo esc_attr($title)?>" href="<?php echo esc_url($href)?>" class="<?php echo esc_attr('home-'. $k)?>">
                                    <?php theshopier_getImage(array(
                                        'alt'   => "Home preview {$k}",
                                        'src'   => esc_url($src)
                                    ));?>
                                </a>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>

            </div>
        </div>

        <script type="text/javascript">
            /* <![CDATA[ */
            (function($) {
                "use strict";
                var __nth_time = 300;
                $(document).ready(function(){
                    if (typeof $.cookie === 'undefined' && $.cookie( 'nth_previewcookie' ) !== '') {
                        console.log($.cookie( 'nth_previewcookie' )+'-thanhdoi');
                        $('.nth-preview-wrapper.nth-close').removeClass('nth-close');
                        $('.nth-preview-wrapper').addClass($.cookie( 'nth_previewcookie' ));
                        if($.cookie( 'nth_previewcookie' ) == 'nth-open') {
                            $('.nth-preview-wrapper .nth-preview-inner').slideDown(__nth_time);
                        }
                    }

                    $('.nth-preview-wrapper').on('click', '.layout-section .layout-button .button', function (e) {
                        e.preventDefault();
                        var act = $(this).data('action');
                        var color = $(this).data('color');
                        $(this).parent().find('.active').removeClass('active');
                        $(this).addClass('active');
                        if(act === 'boxed') {
                            $('.nth-preview-wrapper .layout-background .bg-item.bg_repeat1').trigger('click');
                            $('body #body-wrapper').addClass('container').css({'padding': 0, 'background-color': '#'+color});
                            $('.nth-preview-wrapper .layout-background').slideDown(200);

                        } else {
                            $('body #body-wrapper.container').removeClass('container');
                            $('.nth-preview-wrapper .layout-background').slideUp(200);
                        }

                    })

                    $('.nth-preview-wrapper').on('click', '.layout-section .layout-background .bg-item', function (e) {
                        e.preventDefault();
                        if($('.nth-preview-wrapper .layout-section .layout-button .button.boxed').hasClass('active')) {
                            $(this).parent().find('.bg-item.active').removeClass('active');
                            $(this).addClass('active')
                            var json = $(this).data('bg');
                            console.log(json);
                            $('body').css(json);
                        }
                    });

                    $('.nth-preview-wrapper').on('click', '.button-wrapper .preview-toggle', function (e) {
                        e.preventDefault();
                        $('.nth-preview-wrapper').toggleClass('nth-close');

                        if( typeof $.cookie === 'function' ) {
                            var act = 'nth-open';
                            if( $('.nth-preview-wrapper.nth-close').length > 0 ) {
                                act = 'nth-close';
                                $('.nth-preview-wrapper .nth-preview-inner').slideUp(__nth_time);
                            } else {
                                $('.nth-preview-wrapper .nth-preview-inner').slideDown(__nth_time);
                            }
                            $.cookie('nth_previewcookie', act, { path: '/' });
                        }
                    });

                    $('.nth-preview-wrapper').on('change', '#theshopier_preview_font', function (e) {
                        var __val = $(this).val();
                        var __gg_link = 'https://fonts.googleapis.com/css?family=' + $(this).val();
                        if( $('#theshopier_heading_font').length ) {
                            $('#theshopier_heading_font').attr('href', __gg_link);
                        } else {
                            var __gg_scr = document.createElement('link');
                            __gg_scr.rel = "stylesheet";
                            __gg_scr.type = "text/css";
                            __gg_scr.id = "theshopier_heading_font";
                            __gg_scr.href = __gg_link;
                            $("head").append(__gg_scr);
                        }

                        $.ajax({
                            type: 'GET',
                            dataType: 'json',
                            url: theshopier_data['templ_url'] + '/js/preview_font.json',
                            success: function (data) {
                                $(data.font_1).css({'font-family': __val});
                            }
                        });

                    });

                });
            })(jQuery);
            /* ]]> */
        </script>
        <?php
    }

}